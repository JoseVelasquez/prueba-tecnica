﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Vista.Index" %>

<!DOCTYPE html5>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulario de contacto</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

    <style>
        .borde-r {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-right: 0px;
            width: 41px;
        }
    </style>
</head>
<body>
    <form id="FrmContacto" runat="server" class="mt-4">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container col-lg-4 col-md-10 col-11 bg-light card">
                    <div class="card-header row justify-content-center text-white bg-dark">
                        <h4>Contactenos</h4>
                    </div>
                    <div class="card-body">
                        <div class="row mb-4 mt-4">

                            <!-- Nombre -->
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text borde-r"><i class="fa fa-user"></i></span>
                                        <asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" placeholder="Nombre completo" required></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <!-- Pais -->
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text borde-r"><i class="fa fa-globe-americas"></i></span>
                                        <asp:DropDownList ID="ddlPais" CssClass="form-control" runat="server" DataTextField="Nombre" required></asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <!-- Celular -->
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text borde-r"><i class="fa fa-phone-alt"></i></span>
                                        <asp:TextBox ID="txtCelular" runat="server" CssClass="form-control" placeholder="Celular" required TextMode="Number"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <!-- Correo -->
                            <div class="col-md-12 mb-3">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text borde-r"><i class="fa fa-at"></i></span>
                                        <asp:TextBox ID="txtCorreo" runat="server" CssClass="form-control" placeholder="Correo electrónico" required TextMode="Email"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <!-- Validación -->
                            <div class="col-md-12 mb-3">
                                <div class="form-group form-group-sm">
                                    <asp:Label ID="lblValidación" runat="server" Text="" ForeColor="Red" Visible="false">Debe seleccionar un país.</asp:Label>
                                </div>
                            </div>

                        </div>

                        <!-- Enviar -->
                        <div class="row">
                            <div class="col-md-6 mx-auto">
                                <div class="form-group">
                                    <asp:Button ID="btnEnviar" runat="server" Text="Enviar" CssClass="btn-secondary form-control" OnClick="btnEnviar_Click" />
                                </div>
                            </div>
                            <div class="col-md-6 mx-auto">
                                <div class="form-group">
                                    <a class="btn btn-secondary form-control" href="Datos.aspx">Datos</a>
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="msjExito" class="alert alert-success alert-dismissible" visible="false">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            Datos guardados.
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnEnviar" />
                <asp:PostBackTrigger ControlID="ddlPais" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
