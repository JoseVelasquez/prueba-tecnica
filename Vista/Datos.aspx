﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Datos.aspx.cs" Inherits="Vista.Datos" %>

<!DOCTYPE html5>

<html>
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Formulario de contacto</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

    <style>
        .borde-r {
            border-top-right-radius: 0px;
            border-bottom-right-radius: 0px;
            border-right: 0px;
            width: 41px;
        }
    </style>
</head>
<body>
    <form id="FrmDatos" runat="server" class="mt-4">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="container col-lg-6 col-md-10 col-11 bg-light card">
                    <div class="card-header row justify-content-center text-white bg-dark">
                        <h4>Datos Registrados</h4>
                    </div>
                    <div class="card-body">
                        <div class="row mb-4 mt-4">
                            <asp:GridView ID="gvDatos" runat="server" AutoGenerateColumns="False" CssClass="col-md-12 table table-striped table-bordered">
                                <Columns>
                                    <asp:BoundField DataField="id" HeaderText="ID" />
                                    <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                                    <asp:BoundField DataField="pais" HeaderText="País" />
                                    <asp:BoundField DataField="celular" HeaderText="Celular" />
                                    <asp:BoundField DataField="correo" HeaderText="Correo" />
                                </Columns>
                            </asp:GridView>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <a class="btn btn-secondary form-control" href="Index.aspx">Volver</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
