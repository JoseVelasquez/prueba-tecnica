﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelo;
using Entidad;

namespace Vista
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddlPais.DataSource = Modelo.Pais.getPaises();
                ddlPais.DataBind();
                ddlPais.Items.Insert(0, "Seleccione país");
                ddlPais.SelectedIndex = -1;
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (ddlPais.SelectedIndex > 0)
            {
                Entidad.Contacto.Guardar(new Modelo.Contacto
                {
                    nombre = txtNombre.Text,
                    pais = ddlPais.SelectedItem.Text,
                    celular = txtCelular.Text,
                    correo = txtCorreo.Text
                });

                LimpiezaCampos();
                msjExito.Visible = true;
            }
            else
            {
                lblValidación.Visible = true;
            }
        }

        public void LimpiezaCampos()
        {
            lblValidación.Visible = false;
            txtNombre.Text = string.Empty;
            ddlPais.SelectedIndex = 0;
            txtCelular.Text = string.Empty;
            txtCorreo.Text = string.Empty;
        }
    }
}