USE [PTecnica]
GO
/****** Object:  Table [dbo].[Contactoes]    Script Date: 16/12/2020 12:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contactoes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](max) NULL,
	[pais] [nvarchar](max) NULL,
	[celular] [nvarchar](max) NULL,
	[correo] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Contactoes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
