﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Modelo
{
    public class Contacto
    {
        [Key]
        public int id { get; set; }
        public string nombre { get; set; }
        public string pais { get; set; }
        public string celular { get; set; }
        public string correo { get; set; }
    }
}
