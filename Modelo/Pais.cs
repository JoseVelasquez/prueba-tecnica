﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;

namespace Modelo
{
    public class Pais
    {
        public string Sigla { get; set; }
        public string Nombre { get; set; }

        public static List<Pais> getPaises()
        {
            List<Pais> paises = new List<Pais>();
            string url = "http://country.io/names.json";
            var json = new WebClient().DownloadString(url);
            dynamic val = JsonConvert.DeserializeObject(json);

            foreach(var i in val){
                string valor = Convert.ToString(i);
                valor = valor.Replace(@"\", "");
                valor = valor.Replace(@"""", "");
                paises.Add(new Pais { 
                    Sigla = valor.Split(':')[0],
                    Nombre = valor.Split(':')[1]
                });
            }
            return paises.OrderBy(x => x.Nombre).ToList();
        }
    }
}
