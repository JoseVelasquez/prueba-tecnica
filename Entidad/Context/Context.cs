﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Modelo;

namespace Entidad.Context
{
    public class Context: DbContext
    {
        public Context(): base("Conexion"){
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Modelo.Contacto> Contactos { get; set; }

        protected void onModelCreating(DbModelBuilder modelBuilder){
            modelBuilder.Entity<Modelo.Contacto>().ToTable("Contacto");
            base.OnModelCreating(modelBuilder);
        }
    }
}
