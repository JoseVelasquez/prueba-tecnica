﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Modelo;

namespace Entidad
{
    public class Contacto
    {
        public static void Guardar(Modelo.Contacto contacto)
        {
            try
            {
                using (Context.Context context = new Context.Context())
                {
                    context.Contactos.AddOrUpdate(contacto);
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static List<Modelo.Contacto> Listar()
        {
            Context.Context context = new Context.Context();
            List<Modelo.Contacto> contactos = new List<Modelo.Contacto>();
            contactos = context.Contactos.ToList();
            return contactos;
        }
    }
}
